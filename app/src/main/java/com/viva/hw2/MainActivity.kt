package com.viva.hw2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val stringOne = "izvēlēts!"
        val stringTwo = " ne"

        btnThatCallsSecondActivity.setOnClickListener{
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }



        // Set up the alert builder
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Check that box!")

        // Add a checkbox list
        val people = arrayOf("Viva", "Reinis", "Aivis")
        val checkedItems = booleanArrayOf(false, false, false, true, false)

        builder.setMultiChoiceItems(people, checkedItems) { dialog, which, isChecked ->
            if (isChecked){
                Toast.makeText(this,people[which]+" "+stringOne, Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this,people[which]+stringTwo+stringOne, Toast.LENGTH_SHORT).show()
            }
        }


        builder.setPositiveButton("Labi") { dialog, which ->
            Toast.makeText(this, "Labi", Toast.LENGTH_SHORT).show()
        }

        builder.setNegativeButton("Atcelt"){dialog, which ->
            Toast.makeText(this, "Atcelt", Toast.LENGTH_SHORT).show()
        }

        val dialog = builder.create()

        btnThatOpensDialogBoxx.setOnClickListener {
            dialog.show()
        }


    }
}
